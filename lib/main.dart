import 'package:flutter/material.dart';
import 'package:food_app/ui/screens/splash_screen.dart';
import 'package:food_app/ui/values/strings.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: APP_NAME,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff1abbe7),
        iconTheme: IconThemeData(color: Colors.white),
        primaryTextTheme: TextTheme(
          body1: TextStyle(color: Colors.white),
        ),
        appBarTheme: AppBarTheme(),
        textTheme: TextTheme(
          body1: TextStyle(color: Colors.white),
          title: TextStyle(color: Colors.white),
        ),
      ),
      home: SplashScreen(),
    );
  }
}
