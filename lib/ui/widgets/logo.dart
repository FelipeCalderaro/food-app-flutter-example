import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      child: Image.asset(
        "assets/images/tastylogo.png",
        fit: BoxFit.fitHeight,
        color: Colors.white,
      ),
    );
  }
}
