import 'package:flutter/material.dart';

class ImageCard extends StatelessWidget {
  double height;
  double width;
  String title;
  String imageUrl;
  Function onPressed;
  BoxFit fit;
  double fontSize;
  bool resizeScaleFactory;
  ImageCard({
    @required this.height,
    @required this.width,
    @required this.onPressed,
    @required this.imageUrl,
    @required this.title,
    @required this.fit,
    @required this.fontSize,
    this.resizeScaleFactory = false,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Padding(
        padding: const EdgeInsets.all(3.0),
        child: Card(
          color: Theme.of(context).scaffoldBackgroundColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          elevation: 5.3,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                height: height,
                width: width,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: FadeInImage.assetNetwork(
                    height: double.infinity,
                    width: double.infinity,
                    placeholder: "assets/gifs/loading.gif",
                    // placeholder: "",
                    image: imageUrl,
                    fit: fit,
                    fadeOutCurve: Curves.easeOut,
                    fadeInCurve: Curves.easeIn,
                  ),
                ),
              ),
              Container(
                height: height,
                width: width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(
                    colors: [Colors.black, Colors.transparent],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                ),
              ),
              Container(
                width: width,
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  title,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: resizeScaleFactory
                        ? fontSize / MediaQuery.of(context).textScaleFactor
                        : fontSize,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
