import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/models/last_recipes.dart';
import 'package:food_app/ui/screens/web_view_screen.dart';
import 'package:food_app/ui/values/strings.dart';
import 'package:food_app/ui/values/values.dart';
import 'package:food_app/ui/widgets/image_card.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;

class SearchScreen extends SearchDelegate {
  @override
  String get searchFieldLabel => "Buscar";

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return themeData.copyWith(
      appBarTheme: AppBarTheme(
        color: themeData.primaryColor,
      ),
      primaryTextTheme: themeData.primaryTextTheme,
      inputDecorationTheme: themeData.inputDecorationTheme.copyWith(
        labelStyle: TextStyle(color: Colors.white),
        hintStyle: TextStyle(color: Colors.white),
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: query != ""
            ? Icon(
                Icons.clear,
                color: Colors.white,
              )
            : Icon(
                Icons.search,
                color: Colors.white,
              ),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.arrow_back_ios,
        color: Colors.white,
      ),
      onPressed: () => Navigator.pop(context),
    );
  }

  @override
  Widget buildResults(BuildContext context) =>
      ShowResultsGridView(query: query);

  @override
  Widget buildSuggestions(BuildContext context) {
    return Center(
      child: Text(
        "Ainda não há resultados, pesquise algo :)",
        style: TextStyle(
          color: Colors.black,
          fontSize: 20,
        ),
      ),
    );
  }
}

class ShowResultsGridView extends StatefulWidget {
  String query = "";
  ShowResultsGridView({
    this.query,
  });
  @override
  _ShowResultsGridViewState createState() => _ShowResultsGridViewState();
}

class _ShowResultsGridViewState extends State<ShowResultsGridView> {
  List<LastRecipes> recipes = List();
  Future<dynamic> getRecipes() async {
    // Get recipes using webscrapping
    try {
      var response =
          await http.Client().get("$BASE_URL/search?q=${widget.query}");
      if (response.statusCode == 200) {
        List<LastRecipes> recipesInfo = List();

        var document = parse(response.body);
        var recipesDiv =
            document.getElementsByClassName("xs-block-grid-2 sm-block-grid-4");
        var recipesA =
            recipesDiv[0].getElementsByClassName("feed-item analyt-unit-tap");

        recipesA
            .removeLast(); // The last one come with some type off odd error... so i remove it

        for (var recipe in recipesA) {
          recipesInfo.add(
            LastRecipes(
              title: recipe
                  .querySelector("div.block-grid__item > h6.item-title")
                  .text,
              thumbnail: recipe
                  .querySelector(
                      "div.block-grid__item > div.image-wrapper > amp-img")
                  .attributes["src"],
              link: recipe.attributes["href"],
            ),
          );
        }
        return recipesInfo;
      }
    } catch (e) {
      throw e;
    }
  }

  void getResults() async {
    try {
      recipes = await getRecipes();
      setState(() {});
    } catch (e) {
      throw e;
    }
  }

  @override
  void initState() {
    super.initState();
    getResults();
  }

  @override
  Widget build(BuildContext context) {
    return recipes.isEmpty
        ? Center(
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
            ),
          )
        : GridView.count(
            crossAxisCount: 2,
            childAspectRatio: (9 / 12),
            children: List.generate(
              recipes.length,
              (index) => ImageCard(
                height: double.infinity,
                width: double.infinity,
                title: recipes[index].title,
                imageUrl: recipes[index].thumbnail,
                fit: BoxFit.fitHeight,
                fontSize: 18,
                onPressed: () => Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => WebViewScreen(
                      url: recipes[index].link,
                    ),
                  ),
                ),
              ),
            ),
          );
  }
}
