import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/models/last_recipes.dart';
import 'package:food_app/ui/screens/search_screen.dart';
import 'package:food_app/ui/screens/web_view_screen.dart';
import 'package:food_app/ui/values/strings.dart';
import 'package:food_app/ui/values/values.dart';
import 'package:food_app/ui/widgets/image_card.dart';
import 'package:food_app/ui/widgets/logo.dart';
import 'package:html/parser.dart';

import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // Variables
  List<LastRecipes> recipesInfo = List();
  //  Functions
  void assignRecipes() async {
    try {
      recipesInfo = await getRecipes();
      setState(() {});
    } catch (e) {
      throw e;
    }
  }

  Future<dynamic> getRecipes() async {
    // Get recipes using webscrapping
    try {
      var response = await http.Client().get("$BASE_URL");
      if (response.statusCode == 200) {
        List<LastRecipes> recipesInfo = List();

        var document = parse(response.body);
        var recipesDiv = document.getElementsByClassName(
            "recent-recipes__list xs-block-grid-2 sm-block-grid-4");
        var recipesA =
            recipesDiv[0].getElementsByClassName("feed-item analyt-unit-tap");
        recipesA
            .removeLast(); // The last one come with some type off odd error... so i remove it

        for (var recipe in recipesA) {
          recipesInfo.add(
            LastRecipes(
              title: recipe
                  .querySelector("div.block-grid__item > h6.item-title")
                  .text,
              thumbnail: recipe
                  .querySelector(
                      "div.block-grid__item > div.image-wrapper > amp-img")
                  .attributes["src"],
              link: recipe.attributes["href"],
            ),
          );
        }

        return recipesInfo;
      }
    } catch (e) {
      throw e;
    }
  }

  @override
  void initState() {
    super.initState();
    assignRecipes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     homeViewModel.getLastRecipes();
      //   },
      // ),
      appBar: AppBar(
        title: Logo(),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () => showSearch(
              context: context,
              delegate: SearchScreen(),
            ),
          ),
        ],
      ),
      body: recipesInfo.isEmpty
          ? Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                    Theme.of(context).primaryColor),
              ),
            )
          : ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(DEFAULT_PADDING_MEDIUM),
                  child: Text(
                    "Últimas receitas",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                GridView.count(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 2,
                  childAspectRatio: (9 / 12),
                  children: List.generate(
                    recipesInfo.length,
                    (index) => ImageCard(
                      height: double.infinity,
                      width: double.infinity,
                      title: recipesInfo[index].title,
                      imageUrl: recipesInfo[index].thumbnail,
                      fit: BoxFit.fitHeight,
                      fontSize: 18,
                      onPressed: () => Navigator.push(
                        context,
                        CupertinoPageRoute(
                          builder: (context) => WebViewScreen(
                            url: recipesInfo[index].link,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
