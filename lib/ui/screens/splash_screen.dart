import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_app/ui/screens/home.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool showMessage = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      showMessage = true;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff8f9f9),
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Center(child: Image.asset("assets/gifs/cooking.gif")),
          Positioned(
            top: 16,
            left: 16,
            child: showMessage == false
                ? Container()
                : Container(
                    height: 160,
                    width: 180,
                    child: Image.asset(
                      "assets/images/tastylogo.png",
                      fit: BoxFit.fill,
                    ),
                  ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: showMessage == false
                ? Container()
                : Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                    height: MediaQuery.of(context).size.height * 0.18,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Bem vindo, sinta-se a vontade.",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
          ),
        ],
      ),
      floatingActionButton: showMessage == false
          ? Container()
          : FloatingActionButton.extended(
              backgroundColor: Colors.white,
              label: Icon(
                Icons.arrow_forward_ios,
                color: Theme.of(context).primaryColor,
              ),
              icon:Text(
                "Continuar",
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 18,
                ),
              ), 
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => HomeScreen(),
                  ),
                );
              },
            ),
    );
  }
}
