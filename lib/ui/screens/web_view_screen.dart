import 'package:flutter/material.dart';
import 'package:food_app/ui/widgets/logo.dart';
import 'package:share/share.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  String url;
  WebViewScreen({
    @required this.url,
  });
  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  bool onLoad = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Logo(),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.share,
              color: Colors.white,
            ),
            onPressed: () {
              Share.share("Look at this recipie in ${widget.url}\n :)");
            },
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size(double.infinity, 16),
          child: onLoad == true
              ? LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Color(0xfff9c3e7)),
                )
              : Container(),
        ),
      ),
      body: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        initialUrl: widget.url,
        onPageFinished: (a) {
          onLoad = false;
          setState(() {});
        },
      ),
    );
  }
}
