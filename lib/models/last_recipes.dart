// To parse this JSON data, do
//
//     final lastRecipes = lastRecipesFromJson(jsonString);

import 'dart:convert';

List<LastRecipes> lastRecipesFromJson(String str) => List<LastRecipes>.from(json.decode(str).map((x) => LastRecipes.fromJson(x)));

String lastRecipesToJson(List<LastRecipes> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LastRecipes {
    String title;
    String thumbnail;
    String link;

    LastRecipes({
        this.title,
        this.thumbnail,
        this.link,
    });

    factory LastRecipes.fromJson(Map<String, dynamic> json) => LastRecipes(
        title: json["title"],
        thumbnail: json["thumbnail"],
        link: json["link"],
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "thumbnail": thumbnail,
        "link": link,
    };
}
